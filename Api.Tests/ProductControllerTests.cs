﻿using Api.Controllers;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Services.Contracts;
using Services.Models;
using Xunit;

namespace Api.Tests
{
	public class ProductControllerTests
	{
		[Fact]
		public async void When_product_id_does_not_match_expect_Bad_Request()
		{
			var serviceMock = Mock.Of<IProductsService>();
			var productController = new ProductsController(serviceMock);

			var result = await productController.Update(System.Guid.NewGuid(), new Product());
			result.Should().BeOfType<BadRequestObjectResult>();
		}
		
		[Fact]
		public async void When_product_id_does_match_expect_service_to_be_called()
		{
			var serviceMock = Mock.Of<IProductsService>();
			var productController = new ProductsController(serviceMock);

			var guid = System.Guid.NewGuid();
			var product = new Product { Id = guid };
			var result = await productController.Update(guid, product);
			Mock.Get(serviceMock).Verify(i => i.UpsertAsync(guid, product));
		}
	}
}
