﻿using DataAccess.Contracts;
using DataAccess.Entities;
using Services.Contracts;
using Services.Extensions;
using Services.Models;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Services
{
	public class ProductOptionsService : IProductOptionsService
	{
		private readonly IProductsRepository productRepository;
		private readonly IProductOptionsRepository productOptionsRepository;
		private readonly IProductOptionsValidator productOptionsValidator;


		public ProductOptionsService(IProductOptionsRepository productOptionsRepository,
									IProductsRepository productRepository,
									IProductOptionsValidator productOptionsValidator)
		{
			this.productOptionsRepository = productOptionsRepository;
			this.productRepository = productRepository;
			this.productOptionsValidator = productOptionsValidator;
		}


		public async Task<ProductOptions> GetOptionsForProductIdAsync(Guid productId)
		{
			var productOptions = await this.productOptionsRepository.GetAllForProductAsync(productId);
			return productOptions.MapEntitiesToModels();
		}


		public async Task<ProductOption> GetProductOptionAsync(Guid productId, Guid productOptionId)
		{
			var productOption = await GetProductOptionEntityAsync(productId, productOptionId);
			return productOption.MapEntityToModel();
		}


		public async Task CreateAsync(Guid productId, ProductOption productOption)
		{
			Validate(productOption);

			var existingProduct = await this.productRepository.GetProductByIdAsync(productId);
			if (existingProduct == null)
			{
				throw new XeroException($"The existing product {productId} does not exist");
			}

			if (productOption.Id != Guid.Empty)
			{
				var existingOption = await this.productOptionsRepository.GetProductOptionAsync(productId, productOption.Id);
				if (existingOption != null)
				{
					throw new XeroException($"The option {productOption.Id} already exists for product {productId}");
				}
			}
			
			await CreateAsyncIfNotExist(productOption, productOption.Id);
		}


		private Task CreateAsyncIfNotExist(ProductOption productOption, Guid productOptionId)
		{
			var entity = productOption.MapModelToEntity(productOptionId);
			return this.productOptionsRepository.CreateAsync(entity);
		}


		public async Task<bool> UpsertAsync(Guid productId, Guid productOptionId, ProductOption productOption)
		{
			Validate(productOption);

			var existingProductOption = await this.productOptionsRepository.GetProductOptionAsync(productId, productOption.Id);
			if (existingProductOption == null)
			{
				await this.CreateAsyncIfNotExist(productOption, productOptionId);
				return true;
			}

			productOption.CopyPropertiesToEntity(existingProductOption);
			await this.productOptionsRepository.UpdateAsync(existingProductOption);
			return false;
		}
		

		public async Task DeleteAsync(Guid productId, Guid productOptionId)
		{
			var existingProduct = await GetProductOptionEntityAsync(productId, productOptionId);
			await this.productOptionsRepository.DeleteAsync(existingProduct);
		}


		private void Validate(ProductOption model)
		{
			var result = this.productOptionsValidator.Validate(model);
			if (!result.IsValid)
			{
				throw new XeroException("Product option was not valid");
			}
		}


		private async Task<ProductOptionEntity> GetProductOptionEntityAsync(Guid productId, Guid productOptionId)
		{
			var productOption = await this.productOptionsRepository.GetProductOptionAsync(productId, productOptionId);
			if (productOption == null)
			{
				throw new XeroException($"Product option was not found for product {productId}, option {productOptionId}");
			}

			return productOption;
		}

		public async Task DeleteAllAsync(Guid productId)
		{
			var optionsToRemove = await this.productOptionsRepository.GetAllForProductAsync(productId);
			if (!optionsToRemove.Any())
			{
				return;
			}

			await this.productOptionsRepository.DeleteAsync(optionsToRemove);
		}
	}
}
