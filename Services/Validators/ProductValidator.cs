﻿using FluentValidation;
using Services.Contracts;
using Services.Models;

namespace Services
{
	public class ProductValidator : AbstractValidator<Product>, IProductValidator
	{
		public ProductValidator()
		{
			RuleFor(i => i.Name).NotEmpty();
			RuleFor(i => i.DeliveryPrice).GreaterThan(0);
			RuleFor(i => i.Description).NotEmpty();
			RuleFor(i => i.Price).GreaterThan(0);
		}
	}
}
