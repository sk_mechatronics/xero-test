﻿using FluentValidation;
using Services.Contracts;
using Services.Models;

namespace Services
{
	public class ProductOptionsValidator : AbstractValidator<ProductOption>, IProductOptionsValidator
	{
		public ProductOptionsValidator()
		{
			RuleFor(i => i.ProductId).NotEmpty().Must(pid => pid != System.Guid.Empty);
			RuleFor(i => i.Name).NotEmpty();
			RuleFor(i => i.Description).NotEmpty();
		}
	}
}
