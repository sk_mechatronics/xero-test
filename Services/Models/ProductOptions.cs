﻿using System.Collections.Generic;

namespace Services.Models
{
	public class ProductOptions
	{
		private readonly List<ProductOption> items;

		public IReadOnlyList<ProductOption> Items => items;

		public ProductOptions()
		{
			this.items = new List<ProductOption>();
		}

		public ProductOptions(List<ProductOption> items)
		{
			this.items = items;
		}
	}
}
