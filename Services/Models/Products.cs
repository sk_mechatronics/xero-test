﻿using System.Collections.Generic;

namespace Services.Models
{
	public class Products
	{
		private readonly List<Product> items;

		public IReadOnlyList<Product> Items => this.items;

		public Products()
		{
			items = new List<Product>();
		}

		public Products(List<Product> items)
		{
			this.items = items;
		}
	}
}