﻿using System;

namespace Services
{
	public class XeroException : Exception
	{
		public XeroException(string message) : base(message)
		{
		}
	}
}
