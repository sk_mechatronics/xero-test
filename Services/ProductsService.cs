﻿using DataAccess.Contracts;
using System;
using Services.Models;
using Services.Contracts;
using System.Threading.Tasks;
using Services.Extensions;
using System.Linq;

namespace Services
{
	public class ProductsService : IProductsService
	{
		private readonly IProductsRepository productRepository;
		private readonly IProductValidator validator;
		private readonly IProductOptionsRepository productOptionsRepository;

		public ProductsService(
			IProductsRepository productContext,
			IProductOptionsRepository productOptionsService,
			IProductValidator validator)
		{
			this.productRepository = productContext;
			this.validator = validator;
			this.productOptionsRepository = productOptionsService;
		}

		public async Task<Products> GetAllAsync(string name = null)
		{
			var trimmedName = name?.Trim();
			var productTask = string.IsNullOrEmpty(trimmedName) 
								? this.productRepository.GetAllAsync()
								: this.productRepository.GetProductMatchingName(trimmedName);

			var products = await productTask;
			return products.MapEntitiesToModels();
		}

		public async Task<Product> GetByIdAsync(Guid productId)
		{
			var productEntity = await this.productRepository.GetProductByIdAsync(productId);
			return productEntity.MapEntityToModel();
		}

		public async Task<Products> GetProductMatchingNameAsync(string name)
		{
			var products = await this.productRepository.GetProductMatchingName(name.ToLower());
			return products.MapEntitiesToModels();
		}

		public async Task CreateAsync(Product product)
		{
			ValidateProductModel(product);

			// Ideally setup PK but need to change Guid to string in entity.
			if (product.Id != Guid.Empty)
			{
				var existingProduct = await this.productRepository.GetProductByIdAsync(product.Id);
				if (existingProduct != null)
				{
					throw new XeroException($"Product with {product.Id} already exists");
				}
			}
			else
			{
				product.Id = Guid.NewGuid();
			}
			
			await CreateAsyncIfNotExists(product);
		}


		public async Task CreateAsyncIfNotExists(Product product)
		{
			var productEntity = product.MapModelToEntity();
			await this.productRepository.CreateProductAsync(productEntity);
		}


		public async Task DeleteAsync(Guid productId)
		{
			var productEntity = await this.productRepository.GetProductByIdAsync(productId);
			if (productEntity == null)
			{
				throw new XeroException($"Product with id {productId} not found");
			}

			await this.productRepository.DeleteProduct(productEntity);
			await this.DeleteOptionsForProduct(productId);
		}

		public async Task<bool> UpsertAsync(Guid id, Product product)
		{
			ValidateProductModel(product);
			var existingEntity = await this.productRepository.GetProductByIdAsync(id);
			if (existingEntity == null)
			{
				await this.CreateAsyncIfNotExists(product);
				return true;
			}

			product.CopyPropertiesToEntity(existingEntity);
			await this.productRepository.UpdateProduct(existingEntity);
			return false;
		}

		private async Task DeleteOptionsForProduct(Guid productId)
		{
			var productOptions = await this.productOptionsRepository.GetAllForProductAsync(productId);
			if (!productOptions.Any())
			{
				return;
			}

			await this.productOptionsRepository.DeleteAsync(productOptions);
		}

		private void ValidateProductModel(Product product)
		{
			var validationResult = this.validator.Validate(product);
			if (!validationResult.IsValid)
			{
				throw new XeroException("Product was not valid");
			}
		}
	}
}
