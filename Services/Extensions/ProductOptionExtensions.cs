﻿using DataAccess.Entities;
using Services.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Services.Extensions
{
	public static class ProductOptionExtensions
	{
		public static ProductOption MapEntityToModel(this ProductOptionEntity productOptionEntity)
		{
			return new ProductOption
			{
				Id = productOptionEntity.Id,
				ProductId = productOptionEntity.ProductId,
				Description = productOptionEntity.Description,
				Name = productOptionEntity.Name
			};
		}

		public static ProductOptions MapEntitiesToModels(this IEnumerable<ProductOptionEntity> productOptionEntities)
		{
			var productOptions = productOptionEntities.Select(entity => entity.MapEntityToModel());
			return new ProductOptions(productOptions.ToList());
		}

		public static ProductOptionEntity MapModelToEntity(this ProductOption productOptionModel, Guid guid)
		{
			return new ProductOptionEntity
			{
				Id = guid,
				ProductId = productOptionModel.ProductId,
				Description = productOptionModel.Description,
				Name = productOptionModel.Name
			};
		}

		public static void CopyPropertiesToEntity(this ProductOption productOptionsModel, ProductOptionEntity entity)
		{
			entity.Id = productOptionsModel.Id;
			entity.ProductId = productOptionsModel.ProductId;
			entity.Description = productOptionsModel.Description;
			entity.Name = productOptionsModel.Name;
		}
	}
}
