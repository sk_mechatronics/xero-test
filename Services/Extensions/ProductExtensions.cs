﻿using DataAccess.Entities;
using Services.Models;
using System.Collections.Generic;
using System.Linq;

namespace Services.Extensions
{
	internal static class ProductExtensions
	{
		public static Product MapEntityToModel(this ProductEntity productEntity)
		{
			return new Product
			{
				Id = productEntity.Id,
				DeliveryPrice = productEntity.DeliveryPrice,
				Description = productEntity.Description,
				Name = productEntity.Name,
				Price = productEntity.Price
			};
		}

		public static Products MapEntitiesToModels(this IEnumerable<ProductEntity> productEntities)
		{
			var products = productEntities.Select(entity => entity.MapEntityToModel());
			return new Products(products.ToList());
		}

		public static ProductEntity MapModelToEntity(this Product productModel)
		{
			return new ProductEntity
			{
				Id = productModel.Id,
				DeliveryPrice = productModel.DeliveryPrice,
				Description = productModel.Description,
				Name = productModel.Name,
				Price = productModel.Price
			};
		}

		public static void CopyPropertiesToEntity(this Product productModel, ProductEntity entity)
		{
			entity.Description = productModel.Description;
			entity.DeliveryPrice = productModel.DeliveryPrice;
			entity.Id = productModel.Id;
			entity.Name = productModel.Name;
			entity.Price = productModel.Price;
		}
	}
}
