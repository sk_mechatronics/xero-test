﻿using FluentValidation.Results;
using Services.Models;

namespace Services.Contracts
{
	public interface IProductValidator 
	{
		ValidationResult Validate(Product product);
	}
}
