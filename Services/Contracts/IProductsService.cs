﻿using Services.Models;
using System;
using System.Threading.Tasks;

namespace Services.Contracts
{
	public interface IProductsService
	{
		Task<Products> GetAllAsync(string name = null);

		Task<Product> GetByIdAsync(Guid productId);

		Task<Products> GetProductMatchingNameAsync(string name);

		Task CreateAsync(Product product);

		Task DeleteAsync(Guid productId);

		Task<bool> UpsertAsync(Guid id, Product product);
	}
}