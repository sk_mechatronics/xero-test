﻿using Services.Models;
using System;
using System.Threading.Tasks;

namespace Services.Contracts
{
	public interface IProductOptionsService
	{
		Task<ProductOptions> GetOptionsForProductIdAsync(Guid productId);

		Task<ProductOption> GetProductOptionAsync(Guid productId, Guid productOptionId);

		Task CreateAsync(Guid productId, ProductOption productOption);

		Task<bool> UpsertAsync(Guid productId, Guid productOptionId, ProductOption productOption);

		Task DeleteAsync(Guid productId, Guid productOptionId);

		Task DeleteAllAsync(Guid productId);
	}
}