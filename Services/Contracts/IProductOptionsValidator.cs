﻿using FluentValidation.Results;
using Services.Models;

namespace Services.Contracts
{
	public interface IProductOptionsValidator
	{
		ValidationResult Validate(ProductOption product);
	}
}
