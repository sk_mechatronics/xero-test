﻿using DataAccess.Contracts;
using DataAccess.Database;
using DataAccess.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataAccess.Context
{
    public class ProductsRepository : IProductsRepository
    {
        private BaseDbContext baseDbContext;
        
        public ProductsRepository(BaseDbContext baseDbContext)
        {
            this.baseDbContext = baseDbContext;
        }

        public Task CreateProductAsync(ProductEntity productEntity)
        {
            this.baseDbContext.Products.Add(productEntity);
            return this.baseDbContext.SaveChangesAsync();
        }

        public Task<List<ProductEntity>> GetAllAsync()
        {
            return this.baseDbContext.Products.ToListAsync();
        }

        public Task<ProductEntity> GetProductByIdAsync(Guid id)
        {
            return this.baseDbContext.Products.FirstOrDefaultAsync(product => product.Id == id);
        }

        public Task<List<ProductEntity>> GetProductMatchingName(string name)
        {
            return this.baseDbContext.Products.Where(product => EF.Functions.Like(product.Name, $"%{name}%")).ToListAsync();
        }

        public Task DeleteProduct(ProductEntity productEntity)
        {
            this.baseDbContext.Products.Remove(productEntity);
            return this.baseDbContext.SaveChangesAsync();
        }

        public Task UpdateProduct(ProductEntity productEntity)
        {
            this.baseDbContext.Products.Update(productEntity);
            return this.baseDbContext.SaveChangesAsync();
        }
    }
}
