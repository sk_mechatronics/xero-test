﻿using DataAccess.Contracts;
using DataAccess.Database;
using DataAccess.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataAccess.Repositories
{
	public class ProductOptionsRepository : IProductOptionsRepository
	{
		private readonly BaseDbContext databaseContext;

		public ProductOptionsRepository(BaseDbContext databaseContext)
		{
			this.databaseContext = databaseContext;
		}

		public Task<List<ProductOptionEntity>> GetAllForProductAsync(Guid productId)
		{
			return databaseContext
					   .ProductOptions
					   .Where(productOption => productOption.ProductId == productId)
					   .ToListAsync();
		}

		public Task<ProductOptionEntity> GetProductOptionAsync(Guid productId, Guid optionId)
		{
			return databaseContext
					   .ProductOptions
					   .FirstOrDefaultAsync(productOption => productOption.Id == optionId && productOption.ProductId == productId);
		}

		public Task CreateAsync(ProductOptionEntity productOptionEntity)
		{
			databaseContext
					   .ProductOptions
					   .Add(productOptionEntity);

			return databaseContext.SaveChangesAsync();
		}

		public Task UpdateAsync(ProductOptionEntity productOptionEntity)
		{
			databaseContext.ProductOptions.Update(productOptionEntity);
			return databaseContext.SaveChangesAsync();
		}

		public Task DeleteAsync(ProductOptionEntity productOptionEntity)
		{
			databaseContext.ProductOptions.Remove(productOptionEntity);
			return databaseContext.SaveChangesAsync();
		}

		public Task DeleteAsync(List<ProductOptionEntity> optionsToRemove)
		{
			databaseContext.ProductOptions.RemoveRange(optionsToRemove);
			return databaseContext.SaveChangesAsync();
		}
	}
}
