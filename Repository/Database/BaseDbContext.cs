﻿using DataAccess.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;

namespace DataAccess.Database
{
	public class BaseDbContext : DbContext
	{
		private readonly DatabaseConfiguration databaseConfig;

		public BaseDbContext(IOptions<DatabaseConfiguration> databaseConfig)
		{
			this.databaseConfig = databaseConfig.Value;
		}

		public DbSet<ProductEntity> Products { get; set; }

		public DbSet<ProductOptionEntity> ProductOptions { get; set; }

		protected override void OnConfiguring(DbContextOptionsBuilder options) => options.UseSqlite(databaseConfig.ConnectionString);
	}
}
