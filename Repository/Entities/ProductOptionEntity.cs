﻿using DataAccess.Contracts;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataAccess.Entities
{
	[Table("ProductOptions")]
	public class ProductOptionEntity : IEntity
	{
		public Guid Id { get; set; }

		public Guid ProductId { get; set; }

		public string Name { get; set; }

		public string Description { get; set; }
	}
}
