﻿using DataAccess.Contracts;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataAccess.Entities
{
	[Table("Products")]
	public class ProductEntity : IEntity
	{
		public Guid Id { get; set; }
		
		public string Name { get; set; }

		public string Description { get; set; }

		public decimal Price { get; set; }

		public decimal DeliveryPrice { get; set; }
	}
}
