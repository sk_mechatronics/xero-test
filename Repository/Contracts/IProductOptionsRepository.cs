﻿using DataAccess.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DataAccess.Contracts
{
	public interface IProductOptionsRepository
	{
		Task<List<ProductOptionEntity>> GetAllForProductAsync(Guid productId);

		Task<ProductOptionEntity> GetProductOptionAsync(Guid productId, Guid optionId);

		Task CreateAsync(ProductOptionEntity productOptionEntity);

		Task UpdateAsync(ProductOptionEntity productOptionEntity);

		Task DeleteAsync(ProductOptionEntity productOptionEntity);

		Task DeleteAsync(List<ProductOptionEntity> optionsToDelete);
	}
}
