﻿using DataAccess.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DataAccess.Contracts
{
	public interface IProductsRepository
	{
		Task<List<ProductEntity>> GetAllAsync();

		Task<ProductEntity> GetProductByIdAsync(Guid id);
		
		Task<List<ProductEntity>> GetProductMatchingName(string name);

		Task CreateProductAsync(ProductEntity productEntity);

		Task DeleteProduct(ProductEntity productEntity);

		Task UpdateProduct(ProductEntity productEntity);
	}
}
