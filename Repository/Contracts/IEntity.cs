﻿using System;

namespace DataAccess.Contracts
{
	public interface IEntity
	{
		Guid Id { get; set; }
	}
}