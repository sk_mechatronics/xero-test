﻿using DataAccess.Context;
using DataAccess.Contracts;
using DataAccess.Database;
using DataAccess.Repositories;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.OpenApi.Models;
using Services;
using Services.Contracts;

namespace Api
{
	public class Startup
	{
		public Startup(IConfiguration configuration)
		{
			Configuration = configuration;
		}

		public IConfiguration Configuration { get; }


		public void ConfigureServices(IServiceCollection services)
		{
			services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
			ConfigureSwaggerServices(services);
			ConfigureApplicationServices(services);
		}

		public void Configure(IApplicationBuilder app, IHostingEnvironment env)
		{
			if (env.IsDevelopment())
			{
				app.UseDeveloperExceptionPage();
			}
			else
			{
				// The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
				app.UseHsts();
			}

			app.UseHttpsRedirection();
			app.UseMvc();

			ConfigureSwaggerApp(app);
		}

		private void ConfigureApplicationServices(IServiceCollection services)
		{
			services.Configure<DatabaseConfiguration>(Configuration.GetSection(nameof(DatabaseConfiguration)));
			services.AddEntityFrameworkSqlite().AddDbContext<BaseDbContext>();

			services.AddSingleton<IProductOptionsValidator, ProductOptionsValidator>();
			services.AddScoped<IProductOptionsRepository, ProductOptionsRepository>();
			services.AddScoped<IProductOptionsService, ProductOptionsService>();
			
			services.AddSingleton<IProductValidator, ProductValidator>();
			services.AddScoped<IProductsRepository, ProductsRepository>();
			services.AddScoped<IProductsService, ProductsService>();
		}

		private void ConfigureSwaggerServices(IServiceCollection services)
		{
			services.AddSwaggerGen(c =>
			{
				c.SwaggerDoc("v1", new OpenApiInfo { Title = "Products API", Version = "v1" });
			});
		}

		private void ConfigureSwaggerApp(IApplicationBuilder app)
		{
			app.UseSwagger();
			app.UseSwaggerUI(c =>
			{
				c.SwaggerEndpoint("/swagger/v1/swagger.json", "Products API V1");
				c.RoutePrefix = string.Empty;
			});
		}
	}
}