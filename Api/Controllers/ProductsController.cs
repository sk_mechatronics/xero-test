﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Services;
using Services.Contracts;
using Services.Models;

namespace Api.Controllers
{
	[Route("api/products")]
	[ApiController]
	public class ProductsController : ControllerBase
	{
		private readonly IProductsService productsService;

		public ProductsController(IProductsService productsService)
		{
			this.productsService = productsService;
		}

		[HttpGet]
		public async Task<Products> Get([FromQuery] string name)
		{
			return await productsService.GetAllAsync(name);
		}

		[HttpGet("{id}")]
		public async Task<ActionResult<Product>> Get(Guid id)
		{
			var product = await productsService.GetByIdAsync(id);
			if (product == null)
			{
				return NotFound();
			}

			return new OkObjectResult(product);
		}

		[HttpPost]
		public async Task<IActionResult> Post(Product product)
		{
			try
			{
				await productsService.CreateAsync(product);
				return Ok();
			}
			catch (XeroException xe)
			{
				return new BadRequestObjectResult(xe.Message);
			}
		}

		[HttpPut("{id}")]
		public async Task<IActionResult> Update(Guid id, Product product)
		{
			if (id != product.Id)
			{
				return new BadRequestObjectResult("Cannot change the Guid for this product. Delete and recreate this product first.");
			}

			var isNew = await productsService.UpsertAsync(id, product);
			return new OkObjectResult(isNew);
		}

		[HttpDelete("{id}")]
		public async Task<IActionResult> Delete(Guid id)
		{
			try
			{
				await productsService.DeleteAsync(id);
				return Ok();
			}
			catch (XeroException xe)
			{
				return new BadRequestObjectResult(xe.Message);
			}
		}
	}
}