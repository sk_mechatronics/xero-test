﻿using Microsoft.AspNetCore.Mvc;
using Services;
using Services.Contracts;
using Services.Models;
using System;
using System.Threading.Tasks;

namespace Api.Controllers
{
	[Route("api/products")]
	[ApiController]
	public class ProductOptionsController : ControllerBase
	{
		private readonly IProductOptionsService productOptionsService;

		public ProductOptionsController(IProductOptionsService productOptionsService)
		{
			this.productOptionsService = productOptionsService;
		}

		[HttpGet("{id}/options")]
		public async Task<ActionResult<ProductOptions>> GetOptions(Guid id)
		{
			return await productOptionsService.GetOptionsForProductIdAsync(id);
		}

		[HttpGet("{id}/options/{optionId}")]
		public async Task<ActionResult<ProductOption>> GetOption(Guid id, Guid optionId)
		{
			try
			{
				var productOption = await this.productOptionsService.GetProductOptionAsync(id, optionId);
				return new OkObjectResult(productOption);
			}
			catch(XeroException xe)
			{
				return new BadRequestObjectResult(xe.Message);
			}
		}

		[HttpPost("{id}/options")]
		public async Task<IActionResult> CreateOption(Guid id, ProductOption option)
		{
			if (option.ProductId != id)
			{
				return new BadRequestObjectResult("The product id in the payload does not match the parameter.");
			}

			try
			{
				await this.productOptionsService.CreateAsync(id, option);
				return this.Ok();
			}
			catch (XeroException xe)
			{
				return new BadRequestObjectResult(xe.Message);
			}
		}

		[HttpPut("{id}/options/{optionId}")]
		public async Task<ActionResult<bool>> UpdateOption(Guid id, Guid optionId, ProductOption option)
		{
			if (option.Id != optionId)
			{
				return new BadRequestObjectResult("The option id in the payload must match the option id in the parameter");
			}

			return await this.productOptionsService.UpsertAsync(id, optionId, option);
		}

		[HttpDelete("{id}/options/{optionId}")]
		public async Task<IActionResult> DeleteOption(Guid id, Guid optionId)
		{
			try
			{
				await this.productOptionsService.DeleteAsync(id, optionId);
				return this.Ok();
			}
			catch(XeroException xe)
			{
				return new NotFoundObjectResult(xe.Message);
			}
		}
	}
}
