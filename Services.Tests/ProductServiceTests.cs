﻿using DataAccess.Contracts;
using DataAccess.Entities;
using FluentValidation.Results;
using Moq;
using Services.Contracts;
using Services.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;

namespace Services.Tests
{
	public class ProductServiceTests
	{

		[Fact]
		public async Task When_deleting_product_expect_options_to_also_be_deleted()
		{
			var productId = Guid.NewGuid();

			var productRepository = Mock.Of<IProductsRepository>();
			var product = new ProductEntity { Id = productId };
			Mock.Get(productRepository).Setup(i => i.GetProductByIdAsync(productId)).ReturnsAsync(product);

			var productOptionRepository = Mock.Of<IProductOptionsRepository>();
			var options = new List<ProductOptionEntity>
			{
				new ProductOptionEntity {Id = Guid.NewGuid()},
				new ProductOptionEntity {Id = Guid.NewGuid()},
			};
			Mock.Get(productOptionRepository).Setup(i => i.GetAllForProductAsync(productId)).ReturnsAsync(options);			

			var validator = Mock.Of<IProductValidator>();
			var validationResult = new ValidationResult();
			Mock.Get(validator).Setup(i => i.Validate(It.IsAny<Product>())).Returns(validationResult);

			var productService = new ProductsService(productRepository, productOptionRepository, validator);
			await productService.DeleteAsync(productId);

			Mock.Get(productRepository).Verify(i => i.DeleteProduct(product));
			Mock.Get(productOptionRepository).Verify(i => i.DeleteAsync(options));
		}

	}
}
