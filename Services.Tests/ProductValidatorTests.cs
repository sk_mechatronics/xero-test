﻿using FluentAssertions;
using Services.Models;
using System;
using System.Linq;
using Xunit;

namespace Services.Tests
{
	public class ProductValidatorTests
	{
		[Fact]
		public void When_description_is_empty_expect_error()
		{
			var validator = new ProductValidator();
			var result = validator.Validate(
				new Product {
					DeliveryPrice = 12.00m, 
					Id = Guid.NewGuid(),
					Name = "Name",
					Price = 12.99m });

			result.IsValid.Should().BeFalse();
			result.Errors.Count().Should().Be(1);
		}

		[Fact]
		public void When_delivery_price_is_negative_expect_error()
		{
			var validator = new ProductValidator();
			var result = validator.Validate(
				new Product
				{
					DeliveryPrice = -12.00m,
					Id = Guid.NewGuid(),
					Description = "Description",
					Name = "Name",
					Price = 12.99m
				});

			result.IsValid.Should().BeFalse();
			result.Errors.Count().Should().Be(1);
		}

	}
}
